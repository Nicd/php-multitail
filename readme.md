php-multitail is a small utility script to tail a bunch of files and display the results compactly. I wrote it because I thought tail's builtin output format for multiple 
files was unusable. The utility launches a tail (or whatever you configure) process for each input file and reads their output until it is killed. Each line is prepended 
with the filename's first part with '.' as a delimiter, so 'foo.access.log' turns into 'foo'.

Example output from my server logs:

    ooo: 217.69.133.68 - - [05/Jul/2013:00:51:25 +0300] "GET /robots.txt HTTP/1.0" 302 0 "-" "Mozilla/5.0 (compatible; Linux x86_64; Mail.RU_Bot/2.0; +http://go.mail.ru/help/robots)"
    stuff: 66.249.75.31 - - [05/Jul/2013:04:11:08 +0300] "GET /robots.txt HTTP/1.1" 404 47 "-" "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"
    stuff: 66.249.75.31 - - [05/Jul/2013:04:11:08 +0300] "GET / HTTP/1.1" 403 135 "-" "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"

Usage:

    php multitail.php /path/to/log/file/or/files [/path/to/more/files [/even/more.log ...]]

You can also use glob patterns. I use it like this:

    php multitail.php /var/log/nginx/*.access.log

