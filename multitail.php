<?php

const TAIL_CMD = 'tail -F %s';

$log_globs = array_slice($argv, 1);

$files = [];
foreach ($log_globs as $glob)
{
    $files += glob($glob);
}

$pipes = [];

if (count($files) == 0)
{
    fwrite(STDERR, 'No files given!');
    return 1;
}

foreach ($files as $file)
{
    $newkey = explode('.', basename($file))[0];
    
    $pipes[$newkey] = popen(sprintf(TAIL_CMD, escapeshellarg($file)), 'r');
    stream_set_blocking($pipes[$newkey], 0);
}

$w = null;
$e = null;

while (true)
{
    $read = $pipes;
    $modified = stream_select($read, $w, $e, null);

    if ($modified > 0)
    {
        foreach ($read as $file => $pipe)
        {
            while ($line = fgets($pipe))
            {
                echo $file, ': ', $line;
            }
        }
    }
}
